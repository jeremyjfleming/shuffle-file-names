# shuffle-file-names
Shuffles the names of the files in a given directory. Please do not use for malware purposes

## Usage
Open the application directly and enter the file path through the application itself or run it through the command line. A batch file is included with a template if you need to do a batch file shuffling or use macros. 

[Latest Binaries](https://github.com/jeremyjfleming/shuffle-file-names/releases/latest)
