﻿using System;
using System.IO;

namespace ShuffleFileNames
{
    class Program
    {
        static void Main(string[] args)
        {
            string inDir;
            if (args.Length != 0)
            {
                inDir = args[0];
            }
            else
            {
                Console.WriteLine("Enter the directory with all of the files to be shuffled. ");
                inDir = Console.ReadLine();
            }
            if (!Directory.Exists(inDir))
            {
                Console.WriteLine("Please use a valid directory");
                Environment.Exit(0);
            }
            var files = Directory.GetFiles(inDir);
            foreach (string file in files)
            {
                var fileExt = Path.GetExtension(file);
                File.Move(file, inDir + "\\" + RandomChars() + fileExt, true);
            }
            Console.WriteLine("Complete");
        }
        static string RandomChars()
        {
            var characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var randomString = "";
            var random = new Random();
            for (int i = 0; i < 25; i++)
            {
                randomString += characters[random.Next(0, 61)];
            }
            return randomString;
        }
    }
}
